 $(document).ready(function(){

    //Hide (Collapse) the toggle containers on load
    $(".toggle-contain").hide(); 
    $(".toggle-plus").show(); 
    

    //Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
    $(".toggle-plus").click(function(){
        var txt = $(this).next().is(':visible') ? 'Click here to see a full list of technology available in this space' : 'Close technology list';
        $(this).text(txt);
        $(this).next().slideToggle("slow");
        return false; //Prevent the browser jump to the link anchor
    
    });
    
    

});