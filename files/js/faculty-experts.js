var JSON_URL = 'http://www.sesp.northwestern.edu/feeds/';

function getDegreeYear(degree) {
    if (degree === null) {
		return -1;
	}
	var matches = degree.match(/.+(\d{4})$/);
	if (matches === null) {
		return -1;
	}
	return parseInt(matches[1], 10);
}

function FacultyExpert(data) {
	var self = this;
	self.id = data.id;
	self.name = data.name;
	self.sortName = data.sortName;
	self.titles = data.titles;
	self.degrees = data.degrees;
	self.summary = data.summary;
	self.img_src = data.img_src;
	
	self.profile_url = ko.computed(function() {
		return "http://www.sesp.northwestern.edu/profile/?p="+self.id;
	});

	for (var i = 0; i < experts.length; i++) {
		if (experts[i].id == self.id) {
			self.categories = experts[i].categories;		
			break;
		}
	}

	self.degrees.sort(function(a, b) {
		yearA = getDegreeYear(a);
		yearB = getDegreeYear(b);
		return yearA - yearB;
	});
}

function get_expert_ids() {
	expert_ids = [];

	for (var i = 0; i < experts.length; i++) {
		expert_ids.push(experts[i].id);
	}

	return expert_ids.join();
}

// helper function to retrieve list of experts to select
function getSelected(allFunc, value, start, count) {
		var selected = [];
		var seenCount = 0;
		$.map(allFunc(), function(expert) {
			// special case: "blank" search == show all
			if (typeof value === "undefined") {
				if (typeof start === "undefined" || (seenCount >= start && selected.length < count)) {
					selected.push(expert);
				}
				seenCount++;
			} else {
				for (var i = 0; i < expert['categories'].length; i++) {
					if (expert['categories'][i] === value) {
						if (typeof start === "undefined" || (seenCount >= start && selected.length < count)) {
							selected.push(expert);
						}
						seenCount++;
					}
				}
			}
		});
		return selected;
}


// define viewmodel
function FacultyExpertsViewModel(id_str) {
	var self = this;

	self.allExperts = ko.observableArray([]);
	self.selectedExperts = ko.observableArray([]);
	self.selectedExpertsCount = ko.observable();
	self.search = ko.observable();
	self.Pager = ko.pager(self.selectedExpertsCount);

	self.Pager().CurrentPage.subscribe(function () {
		self.doSearch();
	});
	self.Pager().PageSize(6);

	self.doSearch = function () {
		var pageSize = self.Pager().PageSize();
		var startIndex = (self.Pager().CurrentPage() - 1) * pageSize;

		// do search once to get total count of results
		var results = getSelected(self.allExperts, self.search());
		self.selectedExpertsCount(results.length);

		// search again for selected window of results
		self.selectedExperts(getSelected(self.allExperts, self.search(), startIndex, pageSize));
	}

	self.selectExpertsByCategory = function(category) {
		self.search(category);
		self.Pager().CurrentPage(1);
		self.doSearch();
	}

	self.showAllExperts = function() {
		self.search(undefined);
		self.Pager().CurrentPage(1);
		self.doSearch();
	}
	
	$.get(JSON_URL,
			{
				type: 'facultyexperts',
				ids: id_str
			},
			function(data) {
				var mappedData = $.map(data, function(item) {
					return new FacultyExpert(item);
				});
				self.allExperts(mappedData);
				self.allExperts.sort(function(a, b) { return a.sortName == b.sortName ? 0 : (a.sortName < b.sortName ? -1 : 1) });
				self.doSearch();
			},
			'json'
		);
}

$(document).ready(function () {
	ko.applyBindings(new FacultyExpertsViewModel(get_expert_ids()));
});

