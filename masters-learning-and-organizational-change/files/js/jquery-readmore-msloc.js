 $(document).ready(function(){

	//Hide (Collapse) the toggle containers on load
	$(".toggle-contain-msloc").hide(); 
	$(".toggle-plus-msloc").show(); 
	

	//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$(".toggle-plus-msloc").click(function(){
		var txt = $(this).prev().is(':visible') ? '+ Read More' : '- Collapse';
		$(this).text(txt);
		$(this).prev().slideToggle("slow");
		return false; //Prevent the browser jump to the link anchor
	
	});
	
	

});