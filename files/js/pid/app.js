var PP_FEED_ID = '1154';

var updateEvents = function() {
  var pp_url = "http://planitpurple.northwestern.edu/feed/json/" + PP_FEED_ID;
  //var pp_url = "data/" + PP_FEED_ID + ".json";
 
  $.ajax({
    url: pp_url,
    dataType: 'json',
    success: function(data) {
      var rows = "";
      for (var i = 0; i < Math.min(5, data.length - 1); i++) {
        var evt = data[i];
        var eventDate = moment(evt['eventdate']);

        var eventTime = evt['start_time_display_format'];
        if (evt['end_time_display_format']) {
          eventTime += ' - ' + evt['end_time_display_format']; 
        }
        
        if (evt['start_time_display_format'] == '12:00 AM') {
            eventTime = 'All day';
        }
        
        rows += `
        <tr>
          <td>${evt['title']}</td>
          <td>${eventDate.format("dddd, MMMM D")}<br/> ${eventTime}</td>
          <td>${evt['address_2']}</td>
        </tr>
        `;
      }
      
      $("#events").html(rows);
    }
  });
};

var updateWeather = function() {
  $.simpleWeather({
    location: 'Evanston, IL',
    woeid: '',
    unit: 'f',
    success: function(weather) {
      html = '<h2><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'</h2>';
      html += '<ul>';
      html += '<li class="currently">'+weather.currently+'</li>';
      html += '<li>'+weather.wind.direction+' '+weather.wind.speed+' '+weather.units.speed+'</li></ul>';
      $("#weather").html(html);
    },
    error: function(error) {
      $("#weather").html('');
    }
  });
};

var updateTime = function() {
  $("#clock").html(moment().format("h:mm A"));
};

// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function() {
  // set time
  updateTime();
  setInterval(updateTime, 1000);
  
  // get weather
  updateWeather();
  setInterval(updateWeather, 60000);
  
  // get events
  updateEvents();
  setInterval(updateEvents, 60000);
});
