// comparator function for sorting any list of objects that has a `name` property.
function nameSort(a, b) { return a.name == b.name ? 0 : (a.name < b.name ? -1 : 1) }

function isEmpty(str) {
		return (!str || 0 === str.length);
}

// helper function to retrieve list of projects to select
function getSelected(allFunc, field, value, start, count) {
		var selected = [];
		var seenCount = 0;
		$.map(allFunc(), function(proj) {
			// special case: "blank" search == show all
			if (typeof field === "undefined") {
				if (typeof start === "undefined" || (seenCount >= start && selected.length < count)) {
					selected.push(proj);
				}
				seenCount++;
			} else {
				for (var i = 0; i < proj[field].length; i++) {
					if (proj[field][i] === value) {
						if (typeof start === "undefined" || (seenCount >= start && selected.length < count)) {
							selected.push(proj);
						}
						seenCount++;
					}
				}
			}
		});
		return selected;
}

function ResearchProject(data) {
	this.title = data.title;
	this.investigatortext = data.investigatortext;
	this.coinvestigatortext = data.coinvestigatortext;
	this.funder = data.funder;
	this.description = data.description;
	this.investigators = data.investigators;
	this.categories = data.categories;

	this.shouldShowCoInvestigator = ko.observable(!isEmpty(this.coinvestigatortext));
}

function ResearchProjectViewModel() {
	var self = this;
	self.allProjects = ko.observableArray([]);
	self.selectedProjects = ko.observableArray([]);
	self.selectedProjectsCount = ko.observable();
	self.search = ko.observable();
	self.searchType = ko.observable();
	self.Pager = ko.pager(self.selectedProjectsCount);

	self.Pager().CurrentPage.subscribe(function () {
		self.doSearch();
	});
	self.Pager().PageSize(6);

	self.doSearch = function () {
		var pageSize = self.Pager().PageSize();
		var startIndex = (self.Pager().CurrentPage() - 1) * pageSize;

		// do search once to get total count of results
		var selectedProjects = getSelected(self.allProjects, self.searchType(), self.search());
		self.selectedProjectsCount(selectedProjects.length);

		// search again for selected window of results
		self.selectedProjects(getSelected(self.allProjects, self.searchType(), self.search(), startIndex, pageSize));
	}

	var mappedProjects = $.map(projectdata, function(item) {
		return new ResearchProject(item);
	});

	self.allProjects(mappedProjects);
	self.selectedProjects(mappedProjects);
	self.selectedProjectsCount(self.selectedProjects().length);

	self.selectProjectsByInvestigator = function(investigator) {
		self.search(investigator);
		self.searchType('investigators');
		self.Pager().CurrentPage(1);
		self.doSearch();
	}

	self.selectProjectsByCategory = function(category) {
		self.search(category);
		self.searchType('categories');
		self.Pager().CurrentPage(1);
		self.doSearch();
	}

	self.showAllProjects = function() {
		self.search(undefined);
		self.searchType(undefined);
		self.Pager().CurrentPage(1);
		self.doSearch();
	}

	self.doSearch();
}
$(document).ready(function() {
  ko.applyBindings(new ResearchProjectViewModel());
});

