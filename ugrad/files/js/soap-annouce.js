

    $(document).ready(function(){
    
        //Hide (Collapse) the toggle containers on load
        $(".category-announce").hide(); 
        $(".collapse-box").hide();
    
        //Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
        $(".collapse-trigger").click(function(){
            $(this).toggleClass("active").next().slideToggle("slow");
            return false; //Prevent the browser jump to the link anchor
        });
        $(".collapse-desc").click(function(){
            $(this).toggleClass("open").prev().slideToggle("slow");
            return false; //Prevent the browser jump to the link anchor
        });
    
    });

