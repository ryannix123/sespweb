$('.single-item').slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5500,
        dots: true,
        speed: 1000,
        slidesToShow: 1
      });
      
$('.spot-slick').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});