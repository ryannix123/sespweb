$(document).ready(function() {
	if (!landscape || landscape.stories.length == 0) {
		return;
	}

	var r = Math.floor(Math.random() * landscape.stories.length);
	var story = landscape.stories[r];
	$("#landscape").html("<a href=\"" + story.link + "\"><img src=\"" + story.image + "\"/></a>");
	$("#landscape-caption").html(story.caption + " <a href=\"" + story.link + "\">[ more ]</a>");
});

$(document).ready(function() {
	if (!profile || profile.stories.length == 0) {
		return;
	}

	var r = Math.floor(Math.random() * profile.stories.length);
	var story = profile.stories[r];
	$("#spotlight-title").html("<a href=\"" + story.link + "\">" + story.title + "</a>");
	$("#spotlight-link").attr("href", story.link);
	$("#spotlight-image").attr("src", story.image);
	$("#spotlight-caption").html(story.caption + " <a href=\"" + story.link + "\">[ more ]</a>");
});
