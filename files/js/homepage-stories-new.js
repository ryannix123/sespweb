$(document).ready(function() {
    if (!landscape || landscape.stories.length == 0) {
        return;
    }

    var r = Math.floor(Math.random() * landscape.stories.length);
    var story = landscape.stories[r];
    $("#landscape").html("<a href=\"" + story.link + "\"><img src=\"" + story.image + "\"/></a>");
    $("#landscape-caption").html(story.caption + " <a href=\"" + story.link + "\"> more &gt;&gt;</a>");
});

$(document).ready(function() {
    if (!profile || profile.stories.length == 0) {
        return;
    }

    // get spotlight 1
    var r1 = Math.floor(Math.random() * profile.stories.length);
    var s1 = profile.stories.splice(r1,1);
    var story1 = s1[0];
    $("#spotlight-title").html("<a href=\"" + story1.link + "\">" + story1.title + "</a>");
    $("#spotlight-link").attr("href", story1.link);
    $("#spotlight-image").attr("src", story1.image);
    $("#spotlight-caption").html(story1.caption + " <a href=\"" + story1.link + "\"> more &gt;&gt;</a>");

    // get spotlight 2
    var r2 = Math.floor(Math.random() * profile.stories.length);
    var s2 = profile.stories.splice(r2,1);
    var story2 = s2[0];
    $("#spotlight2-title").html("<a href=\"" + story2.link + "\">" + story2.title + "</a>");
    $("#spotlight2-link").attr("href", story2.link);
    $("#spotlight2-image").attr("src", story2.image);
    $("#spotlight2-caption").html(story2.caption + " <a href=\"" + story2.link + "\"> more &gt;&gt;</a>");
});

