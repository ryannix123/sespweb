$(function(){
            $('#slides').slides({
                preload: true,
                preloadImage: 'http://www.sesp.northwestern.edu/files/images/slideshow-strategic/fancybox_loading.gif',
                play: 8000,
                pause: 2500,
                hoverPause: true,
                slideSpeed: 850,
                animationStart: function(current){
                    $('.caption').animate({
                        bottom:-87
                    },100);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationStart on slide: ', current);
                    };
                },
                animationComplete: function(current){
                    $('.caption').animate({
                        bottom:0
                    },200);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationComplete on slide: ', current);
                    };
                },
                slidesLoaded: function() {
                    $('.caption').animate({
                        bottom:0
                    },200);
                }
            });
        });